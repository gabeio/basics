#!/usr/bin/env python

from gevent import monkey; monkey.patch_all()
import os, sys, bottle, gevent, json
from bottle import *; from gevent import *; from jinja2 import *

env = Environment(loader=FileSystemLoader(["./_site_/dynamic/jinja"]))

@route('/')
def index():
    yield env.get_template("index2.jinja").render()

@route('/static/<path:path>')
def static(path):
    yield static_file(path, "./_site_/static/")

@route('/<something>')
def anything(**params):
    try:
        params['something']
    except Exception, e:
        print str(e)
    else:
        params={}
        params['something']="anything"
    yield env.get_template("anything.jinja").render(params=params)

run(host='', port=8080, server='gevent')